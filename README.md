# Modul Content
* **Werkzeuge**: Internet Browser (FireFox, Chrome …) und deren Entwicklerwerkzeuge; Editoren (Eclipse, Notepad++, Bluefish, …) oder eigener Editor wie z.B. Dreamweaver, Aptana Studio, OpenBEXI; Übertragungsmittel (ftp, ssh)
* **Internet,Technologie**: Gerätelandschaft, Webplattform, Struktur einer Webseite, Semantisches Markup, Barrierefreiheit in Webseiten (WAI-ARIA), Zeichensatz Codes, Fonts, Formulare, Multimedia (Bilder & Grafiken, Audio & Video), Web-Apps, Single Page, DDR (Device Description Repository), iFrame, Landing Page, XML (eXtended Markup Language),  W3C Standard und Validator, Browser Cache, Mikrodaten, Mikroformate & RDFa (Resource Description Format in Attributes), Datenattribute, Rasterlayout, DOM
* **Tag, Attribut, Selektoren**: HTML5, CSS3, CSS-Präprozessoren, Media Queries, neue CSS3-Konzepte (Mehrspalten Layout, Flexbox, Rasterlayouts)
Web Frameworks: Bootstrap, Responsive Design, Templating in Bezug auf Web Frameworks

# Forder Structure
* **demo-page**: Enthält meine Experimenten Seite für einzelne Aufgaben
* **docs**: Enthält die Dokumentationen des Dozenten
* **mlz**: Die MLZ Prüfung
* **pruefung 1**: Die erste Prüfung
* **pruefung 2**: Die zweite Prüfung